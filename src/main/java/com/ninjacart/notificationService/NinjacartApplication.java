package com.ninjacart.notificationService;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan("com.ninjacart")
public class NinjacartApplication {
    public static void main(String[] args) {
        SpringApplication.run(NinjacartApplication.class , args);
    }
}
