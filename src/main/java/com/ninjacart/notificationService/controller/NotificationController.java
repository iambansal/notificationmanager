package com.ninjacart.notificationService.controller;

import com.ninjacart.notificationService.model.AWSNotification;
import com.ninjacart.notificationService.model.NotificationRequest;
import com.ninjacart.notificationService.model.NotificationResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;


@RestController
public class NotificationController {

    @RequestMapping(value = "/list" , method = RequestMethod.POST)
    public ResponseEntity<NotificationResponse> getNotifications(@RequestBody ArrayList<NotificationRequest> notificationRequestArrayList){
        NotificationResponse response = new NotificationResponse();
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @RequestMapping(value = "/list" , method = RequestMethod.GET)
    public ResponseEntity<NotificationResponse> getListOfNotifications(@RequestBody ArrayList<AWSNotification> awsNotifications){
        return null;
    }

}
