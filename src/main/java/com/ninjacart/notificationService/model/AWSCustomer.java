package com.ninjacart.notificationService.model;

public class AWSCustomer {
    private int asgardUserId;
    private int notificationId;
    private String sentDate;
    private String sentTime;

    public int getAsgardUserId() {
        return asgardUserId;
    }

    public void setAsgardUserId(int asgardUserId) {
        this.asgardUserId = asgardUserId;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public String getSentDate() {
        return sentDate;
    }

    public void setSentDate(String sentDate) {
        this.sentDate = sentDate;
    }

    public String getSentTime() {
        return sentTime;
    }

    public void setSentTime(String sentTime) {
        this.sentTime = sentTime;
    }
}
